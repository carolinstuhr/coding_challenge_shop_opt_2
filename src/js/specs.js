let specs = [
  {
    group: 'general',
    items: [
      { name: 'Model', value: 'DC11' },
      { name: 'Delivery time', value: '2-3 weeks' },
    ],
  },
  {
    group: 'engine',
    items: [{ name: 'bhp', value: '404' }],
  },
  {
    group: 'specials',
    text: 'convertible, leather seats',
  },
]

const container = document.querySelector('.container-box')
renderAllSpecs()

function renderAllSpecs() {
  container.innerHTML = ''
  specs.forEach(renderSpec)
}

function renderSpec(spec) {
  const header = document.createElement('section')
  header.className = spec.group
  header.classList.add('tabs')
  header.innerHTML = `
        <h2 >${spec.group.charAt(0).toUpperCase() + spec.group.slice(1)}</h2>`
  const list = document.createElement('section')
  list.className = spec.group + '-list'
  list.classList.add('details')
  spec.items && list.classList.add('details-list')

  list.innerHTML = `
    ${
      spec.items
        ? `<dl >
         ${spec.items
           .map(
             (item) =>
               `<dt class="list-title">${item.name}</dt>
              <dd>${item.value}</dd>`
           )
           .join('')}
             </dl>`
        : spec.text && `<span>${spec.text}</span>`
    }`
  container.appendChild(header)
  container.appendChild(list)
}
