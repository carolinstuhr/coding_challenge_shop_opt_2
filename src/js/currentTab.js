const general = document.querySelector('.general')
const engine = document.querySelector('.engine')
const specials = document.querySelector('.specials')
const generalList = document.querySelector('.general-list')
const engineList = document.querySelector('.engine-list')
const specialsList = document.querySelector('.specials-list')

let savedTab = localStorage.getItem('tab')
if (savedTab === '' || savedTab === 'general') {
  generalVisible()
} else if (savedTab === 'engine') {
  engineVisible()
} else {
  specialsVisible()
}

general.addEventListener('click', () => {
  generalVisible()
  localStorage.setItem('tab', 'general')
})
engine.addEventListener('click', () => {
  engineVisible()
  localStorage.setItem('tab', 'engine')
})
specials.addEventListener('click', () => {
  specialsVisible()
  localStorage.setItem('tab', 'specials')
})

function generalVisible() {
  general.classList.add('active')
  generalList.classList.remove('hidden')
  engine.classList.remove('active')
  engineList.classList.add('hidden')
  specials.classList.remove('active')
  specialsList.classList.add('hidden')
}

function engineVisible() {
  general.classList.remove('active')
  generalList.classList.add('hidden')
  engine.classList.add('active')
  engineList.classList.remove('hidden')
  specials.classList.remove('active')
  specialsList.classList.add('hidden')
}

function specialsVisible() {
  general.classList.remove('active')
  generalList.classList.add('hidden')
  engine.classList.remove('active')
  engineList.classList.add('hidden')
  specials.classList.add('active')
  specialsList.classList.remove('hidden')
}
